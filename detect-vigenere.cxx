/**
 * @file test_freqs.cxx
 *
 * @author JOL
 */
#include <string>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <algorithm>
#include <iterator>
#include <sstream>
#include "utils.h"

using namespace std;


std::ostream& operator << (std::ostream& O, const vector<float>& B)
{
    unsigned int i = 0;
    O << "Vector : ";
    for (std::vector<float>::const_iterator it = B.begin() ; it != B.end(); ++it) {
        O << " " << (char) ('A'+i) << ":" << *it << " --";
        i++;
    }
    return O;
}

int main( int argc, char** argv )
{
    string s = readInput( cin );
    int size = sizeKey( s );
    if (size > 0) {
        cout << "Taille de la clé : " << sizeKey(s) << endl;
    } else {
        cout << "Impossible de trouver la taille de la clé" << endl;
    }

}