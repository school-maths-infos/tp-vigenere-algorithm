//
// Created by celinederoland on 1/29/16.
//
#include "utils.h"

using namespace std;

int main( int argc, char** argv )
{
    if(argc >= 2) {
        string s = readInput(cin);
        cout << dechiffreVigenere(s, argv[1]) << endl;
    } else {
        cout << "usage : ./test-dechiffre CLE" << endl;
    }
    return 0;
}