# ajoutez vos programmes ci-dessous
PROGS_SRC=test-freqs.cxx filter-az-min.cxx filter-AZ-maj.cxx test-chiffre.cxx test-dechiffre.cxx check-freqs.cxx detect-vigenere.cxx test-coincidence-mutuelle.cxx analyse-vigenere.cxx
SRC=freqs.cxx utils.cxx
OBJ=${SRC:.cxx=.o}
HEADERS=freqs.h utils.h
PROGS=${PROGS_SRC:.cxx=}
CXXFLAGS=-O3 -g -Wall

all: ${PROGS}

check-freqs: check-freqs.cxx ${OBJ} ${HEADERS}
	g++ ${CXXFLAGS} $< ${OBJ} -o $@

test-freqs: test-freqs.cxx ${OBJ} ${HEADERS} 
	g++ ${CXXFLAGS} $< ${OBJ} -o $@

filter-az-min: filter-az-min.cxx ${OBJ} ${HEADERS}
	g++ ${CXXFLAGS} $< ${OBJ} -o $@

filter-AZ-maj: filter-AZ-maj.cxx ${OBJ} ${HEADERS}
	g++ ${CXXFLAGS} $< ${OBJ} -o $@

test-chiffre: test-chiffre.cxx ${OBJ} ${HEADERS}
	g++ ${CXXFLAGS} $< ${OBJ} -o $@

test-dechiffre: test-dechiffre.cxx ${OBJ} ${HEADERS}
	g++ ${CXXFLAGS} $< ${OBJ} -o $@

detect-vigenere: detect-vigenere.cxx ${OBJ} ${HEADERS}
	g++ ${CXXFLAGS} $< ${OBJ} -o $@

test-coincidence-mutuelle: test-coincidence-mutuelle.cxx ${OBJ} ${HEADERS}
	g++ ${CXXFLAGS} $< ${OBJ} -o $@

analyse-vigenere: analyse-vigenere.cxx ${OBJ} ${HEADERS}
	g++ ${CXXFLAGS} $< ${OBJ} -o $@

%.o: %.cxx %.h
	g++ ${CXXFLAGS} -c $<

clean:
	rm -f ${PROGS} ${OBJ}
