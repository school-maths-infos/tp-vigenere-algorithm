--------------------------------------------------------------------------------------------------
Chiffrer avec vigenere :
--------------------------------------------------------------------------------------------------
Le texte en clair est en minuscules
La clé est en majuscules
Le texte sort crypté en majuscules

Méthode utilisée :
string chiffreVigenere( const string & clair, const string & cle )

Exemple :
cat lafontaine.txt | ./filter-az-min | ./test-chiffre HELLO

--------------------------------------------------------------------------------------------------
Déchiffrer avec vigenere :
--------------------------------------------------------------------------------------------------
Le texte chiffré est en majuscules
La clé est en majuscules
Le texte sort en clair en majuscules

Méthode utilisée :
string dechiffreVigenere( const string & chiffre, const string & cle )

Exemple :
cat lafontaine.txt | ./filter-az-min | ./test-chiffre HELLO | ./test-dechiffre HELLO

--------------------------------------------------------------------------------------------------
Calcul des fréquences de lettres et de l'indice de coincidence :
--------------------------------------------------------------------------------------------------
Le texte est en majuscules
Il faut indiquer tous les combiens on prend une lettre, et à quel décalage on commence
On calcule les fréquences des lettres de A à Z dans ce sous texte, puis l'indice de coincidence correspondant

Méthodes utilisées :
subtext( const string & s, int every, int shift )
std::vector<float> frequencies( const std::string & s )
float coincidence( const std::vector<float> & f )

Exemple :
cat lafontaine.txt | ./filter-AZ-maj | ./check-freqs 3 0

--------------------------------------------------------------------------------------------------
Recherche de la taille de la clé :
--------------------------------------------------------------------------------------------------
Le texte chiffré est en majuscules
Pour chaque taille de clé on cherche si l'indice de coincidence est proche de celui du français.
Si il l'est c'est la bonne taille de clé

Méthode utilisée :
int sizeKey(const std::string & s)

Exemple :
cat lafontaine.txt | ./filter-az-min | ./test-chiffre HELLO | ./detect-vigenere

--------------------------------------------------------------------------------------------------
Calcul de l'indice de coincidence mutuelle :
--------------------------------------------------------------------------------------------------
Le texte est en majuscules
Plus l'indice est élevé, plus le texte est proche du français
On donne a la méthode la lettre qu'on veut tester, la position de cette lettre dans la clé, et la taille de la clé
Si on trouve un indice élevé (autour de 0.72) c'est la bonne lettre, sinon ce n'est pas la bonne lettre

Méthode utilisée :
float coincidenceMutuelle( const std::vector<float> & f1, const std::vector<float> & f2, unsigned int shift2)

Exemple :
cat lafontaine.txt | ./filter-az-min | ./test-chiffre HELLO | ./test-coincidence-mutuelle H 0 5

--------------------------------------------------------------------------------------------------
Recherche de la clé :
--------------------------------------------------------------------------------------------------
Le texte chiffré est en majuscules

Méthode utilisée :
string analyseVigenere( const string & s, int sizeOfKey)

Exemple :
cat lafontaine.txt | ./filter-az-min | ./test-chiffre HELLO | ./analyse-vigenere