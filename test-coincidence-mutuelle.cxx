/**
 * @file test_freqs.cxx
 *
 * @author JOL
 */
#include <string>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <algorithm>
#include <iterator>
#include "utils.h"
#include "freqs.h"

using namespace std;


std::ostream& operator << (std::ostream& O, const vector<float>& B)
{
    unsigned int i = 0;
    O << "Vector : ";
    for (std::vector<float>::const_iterator it = B.begin() ; it != B.end(); ++it) {
        O << " " << (char) ('A'+i) << ":" << *it << " --";
        i++;
    }
    return O;
}

int main( int argc, char** argv )
{
    string s = readInput( cin );

    if (argc < 4) {
        cout << "usage : ./test-coincidence-mutuelle caractere indiceDansCle tailleCle" << endl;
    } else {
        int sizeKey = atoi(argv[3]);
        int indiceDansCle = atoi(argv[2]);
        string subS = subtext(s, sizeKey, indiceDansCle);
        string subSClair = dechiffreVigenere(subS, argv[1]);
        vector<float> vFreq = frequencies(subSClair);
        cout << coincidenceMutuelle(frequencies(subSClair),frequenciesFrench()) << endl;
    }


}