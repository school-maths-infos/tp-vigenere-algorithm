/**
 * @file test_freqs.cxx
 *
 * @author JOL
 */
#include <string>
#include <iostream>
#include <iomanip>
#include <cstring>
#include <cstdlib>
#include "utils.h"
#include "freqs.h"

using namespace std;


std::ostream& operator << (std::ostream& O, const vector<float>& B)
{
    unsigned int i = 0;
    O << "Vector : ";
    for (std::vector<float>::const_iterator it = B.begin() ; it != B.end(); ++it) {
        O << " " << (char) ('A'+i) << ":" << *it << " --";
        i++;
    }
    return O;
}

int main( int argc, char** argv )
{
    string s = readInput( cin );
    if (argc < 3) {
        cout << "usage : ./check-freqs tailleCle decalage" << endl;
    } else {
        string subS = subtext(s, atoi(argv[1]), atoi(argv[2]));
        vector<float> vFreq = frequencies(subS);
        cout << vFreq << endl;

        cout << "IC : " << coincidence(vFreq) << endl;
        cout << "ICFrench : " << coincidence(frequenciesFrench()) << endl;
        cout << "ICRandom : " << coincidence(frequenciesRandom()) << endl;
    }
}

