#include <string>
#include <iostream>
#include <sstream>
#include <cmath>
#include "utils.h"
#include "freqs.h"

using namespace std;

string readInput( istream & in )
{
  ostringstream s( ostringstream::out );
  while ( in.good() )
    {
      char c;
      in >> c;
        if (in.good())
            s << c;
    }
  return s.str();
}

string filter_az( const std::string & s )
{
  ostringstream s2( ostringstream::out );
  for ( unsigned int i = 0; i < s.size(); ++i )
    {
      char c = s[ i ];
      if ( ( c >= 'a' ) && ( c <= 'z' ) )
	s2 << c;
      else if ( ( c >= 'A' ) && ( c <= 'Z' ) )
	s2 << (char) ( c-'A'+'a' );
    }
  return s2.str();
}

string filter_AZ( const std::string & s )
{
  ostringstream s2( ostringstream::out );
  for ( unsigned int i = 0; i < s.size(); ++i )
    {
      char c = s[ i ];
      if ( ( c >= 'a' ) && ( c <= 'z' ) )
    	s2 << (char) ( c-'a'+'A' );
      else if ( ( c >= 'A' ) && ( c <= 'Z' ) )
    	s2 << c;
    }
  return s2.str();
}

string subtext( const string & s, int every, int shift )
{
  ostringstream s2( ostringstream::out );
  for ( unsigned int i = shift; i < s.size(); i += every )
    s2 << s[ i ];
   return s2.str(); 
}

string chiffreVigenere( const string & clair, const string & cle )
{
  unsigned int indexCle = 0;
  ostringstream chiffre( ostringstream::out );
  for(unsigned int i = 0; i < clair.size(); i++) {
      chiffre << (char) ('A' + (clair[i]-'a' + cle[indexCle] - 'A') % 26);
      indexCle = (indexCle + 1) % cle.size();
  }

  return chiffre.str();
}

string dechiffreVigenere( const string & chiffre, const string & cle )
{
    unsigned int indexCle = 0;
    ostringstream clair( ostringstream::out );
    for(unsigned int i = 0; i < chiffre.size(); i++) {
        clair << (char) ('A' + ((int)chiffre[i] - cle[indexCle] + 26) % 26);
        indexCle = (indexCle + 1) % cle.size();
    }

    return clair.str();
}


std::vector<float> frequencies( const std::string & s ) {

    std::vector<float> myFreqs(26,0.0);

    for(unsigned int i = 0; i < s.size(); i++) {
        myFreqs[s[i] - 'A'] = myFreqs[s[i] - 'A'] + 1.0;
    }

    for (unsigned int i = 0; i < 26; i++) {
        myFreqs[i] = myFreqs[i] / (s.size());
    }

    return myFreqs;
}

float coincidence( const std::vector<float> & f ) {

    float ic = 0;
    for (std::vector<float>::const_iterator it = f.begin() ; it != f.end(); ++it) {
        ic += (*it * 1000)*((*it * 1000) - 1) / (1000*999);
    }
    return ic;
}

int sizeKey(const std::string & s) {

    float coincidenceFreqFrench = coincidence(frequenciesFrench());
    float coincidenceFreqRandom = coincidence(frequenciesRandom());
    float middle = (coincidenceFreqFrench + coincidenceFreqRandom) / 2;

    bool detected = false;
    int currentIndex = 1;
    int max = s.size() / 10;

    while(!detected && currentIndex <= max) {
        bool shifOk = true;
        int currentShift = 0;
        while(shifOk && currentShift < currentIndex) {
            string subS = subtext(s, currentIndex, currentShift);
            vector<float> vFreq = frequencies(subS);
            float coincidenceVFreq = coincidence(vFreq);

            // Get next element
            if(abs(coincidenceVFreq - coincidenceFreqFrench) > abs(coincidenceVFreq - middle)) {
                shifOk = false;
            } else {
                currentShift++;
            }
        }

        // Get next element
        if(shifOk) {
            detected = true;
        } else {
            currentIndex++;
        }
    }

    if(detected) {
        return currentIndex;
    } else {
        return 0;
    }
}

float coincidenceMutuelle( const std::vector<float> & f1, const std::vector<float> & f2 ) {
    float icm = 0;
    for (int i = 0; i < 26; i++) {
        icm += f1[i]*f2[i];
    }
    return icm;
}

std::string analyseVigenere( const string & s, int sizeOfKey) {

    vector<char> key(sizeOfKey);
    stringstream output;

    //Pour chaque caractere de la clé
    for(int positionInKey = 0; positionInKey < sizeOfKey; positionInKey++) {
        float coincidenceMutuelleMax = 0.0;
        // Pour chaque caractere possible à cette position
        char correctChar = 'A';
        for(char c = 'A'; c <= 'Z'; c++) {

            //J'écris le caractere dans un string pour déchiffrer avec dechiffreVigenere
            stringstream ss;
            std::string k;
            ss << c;
            ss >> k;

            //Je fais une sous-chaine correspondant à cette position dans la clé et je le déchiffre avec le caractère testé
            string subS = subtext(s, sizeOfKey, positionInKey);
            string text = dechiffreVigenere(subS, k);

            //Je calcule l'indice de coincidence mutuelle pour cette partie de texte déchiffrée
            vector<float> tFreq = frequencies(text);
            float coincidenceMutuelleText = coincidenceMutuelle(tFreq, frequenciesFrench());

            //Si il est maximal, c'est que c'est le bon caractère utilisé
            if(coincidenceMutuelleText > coincidenceMutuelleMax) {
                coincidenceMutuelleMax = coincidenceMutuelleText;
                correctChar = c;
            }
        }

        output << correctChar;
    }
    string outstring;
    output >> outstring;
    return outstring;
}
